<%-- 
    Document   : shoppingError
    Created on : Mar 10, 2018, 3:37:34 PM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shoping</title>
    </head>
    <body>
        <h1>Error Occurred!</h1>
        ${message}
        <a href="index.jsp">Home</a>
    </body>
</html>
