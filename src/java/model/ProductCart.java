/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author viquy
 */
public class ProductCart {
    private final List cartItems= new ArrayList();
    
    public ProductCart() {
//        cartItems = new ArrayList();
    }
    
    public List getCartItems(){
        return  cartItems;
    }
    private float amount;
    
    public float getAmount() {
        return amount;
    }
    
    public void setAmount(float amount) {
        this.amount = amount;
    }
    
    private List products;
    
    public List getProducts(){
        List temp = new ArrayList();
        try{
            String hostName = "localhost";
            String dbUser = "sa";
            String dbPassword = "123456";
            String sqlInstanceName = "MSSQLSERVER";
            String database = "OnlineShoppingCart";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");     
                  
            String connectionURL = "jdbc:sqlserver://" + hostName + ":1433" //
                + ";instance=" + sqlInstanceName + ";databaseName=" + database;           
            Connection con = DriverManager.getConnection(connectionURL, dbUser, dbPassword);
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("select * from Products");
            while (rs.next()) {
                Product item = new Product(rs.getInt("productId"), rs.getString("productName"), rs.getString("productType"), rs.getFloat("price"));
                temp.add(item);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return temp;
    }
    
    public void addItem(int productId, String productName, String productType, float price, int quantity) {
        Product item = null;
        boolean match = false;
        for (int i = 0; i < cartItems.size(); i++) {
            if (((Product) cartItems.get(i)).getProductId() == productId) {
                item = (Product) cartItems.get(i);
                setAmount(getAmount() + quantity * item.getProductPrice());
                item.setQuantity(item.getQuantity() + quantity);
                match = true;
                break;
            }
        }
        if (!match) {
            item = new Product();
            item.setProductId(productId);
            item.setProductName(productName);
            item.setProductType(productType);
            item.setProductPrice(price);
            setAmount(getAmount() + quantity * item.getProductPrice());
            item.setQuantity(quantity);
            cartItems.add(item);
        }
    }

    public void removeItem(int productId) {
        for (int i = 0; i < cartItems.size(); i++) {
            Product item = (Product) cartItems.get(i);
            if (item.getProductId() == productId) {
                setAmount(getAmount() - item.getProductPrice() * item.getQuantity());
                cartItems.remove(i);
                break;
            }
        }
    }
}
